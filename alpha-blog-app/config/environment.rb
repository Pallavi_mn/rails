# Load the Rails application.
require_relative "application"

# Initialize the Rails application.
Rails.application.initialize!

#delaing with action view base --simply get rid of that feild with error setting(validation error)

ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
  html_tag.html_safe
end
