class ArticlesController < ApplicationController
  
  before_action :find_article_by_id, only: [:show, :edit, :update, :destroy]
  before_action :require_user, except: [:show, :index]
  before_action :require_same_user, only: [:edit, :update, :destroy]

  def show
  end

  def index
  @articles = Article.paginate(page: params[:page], per_page: 3)
  end

  def new
    @article = Article.new
  end

  # finding the article and displaing the form 
  # upadte works behind to impact on article table with the edited article information
  def edit
  end

  def create
    @article = Article.new(article_params)
    # render plain: params[:article]
    #create action to temporarily grab and hardcode a user to each article that's created(Article.update_all(User.first.id))
    # @article.user = User.first
    @article.user = current_user
    # render plain: @article.inspect
    if @article.save
      flash[:notice] = "Article Created successfully!"
      redirect_to @article
    else
      render 'new'
    end
  end

  def update
    if @article.update(article_params)
      flash[:notice] = "Article Updated successfully!"
      redirect_to @article
    else
      render 'edit'
    end
  end 

  def destroy
  @article.destroy
  redirect_to articles_path
  end


  private
  
  def find_article_by_id
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :description, category_ids: [])
  end

  def require_same_user 
    if current_user != @article.user && !current_user.admin?
      flash[:alert] = "You can only edit or delete your own article"
      redirect_to @article
    end
  end

end
