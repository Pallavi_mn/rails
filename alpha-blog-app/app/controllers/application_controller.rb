class ApplicationController < ActionController::Base
  
  helper_method :current_user, :logged_in?  # these line makes current_user available our all views as well not only controller
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !!current_user # boolean(!!) --simply return true or false based on current_user or not
  end

  # if we restrict from UI but still we can edit delete info by using url path..
  # so to avoid that we validating in require use
  def require_user
    if !logged_in?
      flash[:alert] = "you must be logged in to perform that action"
      redirect_to login_path
    end
  end

end
